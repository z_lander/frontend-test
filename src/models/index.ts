export interface Contact {
  id: string;
  name: string;
  email: string;
  dateCreated: string;
  dateModified: string;
}

export interface ContactsContext {
  contacts: Contact[];
  contact: Contact;
}

export interface ContactsStateSchema {
  states: {
    idle: {};
    loading: {};
    selected: {
      states: {
        loading: {};
        loaded: {};
        failed: {};
        creating: {};
        editing: {};
        edited: {};
        created: {};
        deleted: {};
        deleting: {};
      };
    };
    complete: {};
    failure: {};
  };
}

export type ContactsEvent =
  | { type: 'LOAD'; data: ContactsContext }
  | { type: 'CREATE' }
  | { type: 'SELECT'; contactId: string }
  | { type: 'EDIT' }
  | { type: 'DELETE' }

export type ContactsEventWithData = { type: string; data: ContactsContext };

export type ContactsEventWithId = { type: string; contactId: ContactsContext };