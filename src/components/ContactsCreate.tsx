import React, { useState, SyntheticEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { useMachine } from '@xstate/react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import contactsMachine from '../machines/ContactsMachine';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    actions: {
      textAlign: 'right',
      marginTop: theme.spacing(2)
    },
    button: {
      marginLeft: theme.spacing(2)
    },
    form: {
      display: 'flex',
      flexDirection: 'column'
    },
    status: {
      textAlign: 'center',
      marginTop: theme.spacing(2),
    },
    top: {
      marginTop: theme.spacing(1),
    }
  })
);

function ContactsCreate() {
  const classes = useStyles();
  const history = useHistory();
  const [userName, setUserName] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const [validInput, setValidInput] = useState(true);
  const [current, send] = useMachine(contactsMachine);

  const changeUserNameHandler = (event: SyntheticEvent) => {
    const inputValue = (event.target as HTMLInputElement).value;
    setUserName(inputValue);
  };

  const changeUserEmailHandler = (event: SyntheticEvent) => {
    const inputValue = (event.target as HTMLInputElement).value;
    setUserEmail(inputValue);
  };

  const createContactHandler = () => {
    if (userName.length === 0 || userEmail.length === 0) {
      setValidInput(false);
      return;
    }

    setValidInput(true);
    send('CREATE', {
      name: userName,
      email: userEmail
    });
  };

  const cancelContactHandler = () => {
    history.push('/');
  };

  return (
    <Container maxWidth='sm'>
      <Typography variant='h5' gutterBottom>
        Create user
      </Typography>
      <form className={classes.form}>
        <TextField
          id='name'
          label='Name'
          value={userName}
          onChange={changeUserNameHandler}
        />
        <TextField
          required
          id='email'
          className={classes.top}
          label='Email'
          value={userEmail}
          onChange={changeUserEmailHandler}
        />
        <div className={classes.status}>
          {current.matches('selected.creating') &&
            <CircularProgress size={26} />
          }
          {current.matches('selected.created') &&
            <Alert severity='success'>The user has been created successfully.</Alert>
          }
          {!validInput &&
            <Alert severity='error'>Name and email are required fields.</Alert>
          }
          {current.matches('selected.failed') &&
            <Alert severity='error'>There was an error while creating the user.</Alert>
          }
        </div>
        <div className={classes.actions}>
          <Button className={`create-button ${classes.button}`} onClick={createContactHandler} variant='contained' color='primary'>
            Create
          </Button>
          <Button className={`cancel-button ${classes.button}`} onClick={cancelContactHandler} variant='contained' color='primary'>
            Cancel
          </Button>
        </div>
      </form>
    </Container>
  );
}

export default ContactsCreate;
