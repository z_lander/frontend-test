import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useMachine } from '@xstate/react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import moment from 'moment';
import { Alert } from '@material-ui/lab';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import contactsMachine from '../machines/ContactsMachine';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    actions: {
      marginTop: theme.spacing(2),
      textAlign: 'right'
    },
    button: {
      marginLeft: theme.spacing(2)
    },
    form: {
      display: 'flex',
      flexDirection: 'column'
    },
    status: {
      textAlign: 'center',
      marginTop: theme.spacing(2)
    },
    top: {
      marginTop: theme.spacing(1)
    }
  })
);

function ContactsView() {
  const classes = useStyles();
  const history = useHistory();
  const { id } = useParams();
  const [current, send, service] = useMachine(contactsMachine);
  const { contact } = current.context;

  service.onTransition(state => {
    if (state.matches('idle')) {
      send('SELECT', { contactId: id });
    }
  });

  const cancelContactHandler = () => {
    history.push('/');
  };

  return (
    <Container maxWidth='sm'>
      <Typography variant='h5' gutterBottom>
        Contact details
      </Typography>
      <form className={classes.form}>
        <TextField
          id="name-input"
          label="Name"
          value={contact.name}
          InputProps={{
            readOnly: true,
          }}
        />
        <TextField
          id="email-input"
          className={classes.top}
          label="Email"
          value={contact.email}
          InputProps={{
            readOnly: true,
          }}
        />
        <TextField
          id="date-modified-input"
          className={classes.top}
          label="Date modified"
          value={moment(contact.dateModified).format('MM-DD-YYYY HH:mm')}
          InputProps={{
            readOnly: true,
          }}
        />
        <TextField
          id="date-created-input"
          className={classes.top}
          label="Date created"
          value={moment(contact.dateCreated).format('MM-DD-YYYY HH:mm')}
          InputProps={{
            readOnly: true,
          }}
        />
        <div className={classes.status}>
          {current.matches('selected.loading') &&
            <CircularProgress size={26} />
          }
          {current.matches('selected.failed') &&
            <Alert severity='success'>There was an error while loading the contact view.</Alert>
          }
        </div>
        <div className={classes.actions}>
          <Button className={`cancel-button ${classes.button}`} onClick={cancelContactHandler} variant='contained' color='primary'>
            Cancel
          </Button>
        </div>
      </form>
    </Container>
  );
}

export default ContactsView;
