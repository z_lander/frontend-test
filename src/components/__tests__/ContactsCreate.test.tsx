import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ContactsCreate from '../ContactsCreate';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush
  })
}));

describe('<ContactsCreate />', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<ContactsCreate />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});