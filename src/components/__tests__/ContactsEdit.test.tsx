import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ContactsEdit from '../ContactsEdit';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({
    id: '12345',
  }),
  useRouteMatch: () => ({ url: '/contact/edit/123451' }),
  useHistory: () => ({
    push: () => mockHistoryPush
  })
}));

describe('<ContactsEdit />', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<ContactsEdit />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});