import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ContactsList from '../ContactsList';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({
    id: '12345',
  }),
  useRouteMatch: () => ({ url: '/contact/123451' }),
  useHistory: () => ({
    push: () => mockHistoryPush
  })
}));

describe('<ContactsList />', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<ContactsList />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});