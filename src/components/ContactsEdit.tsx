import React, { useState, useEffect, SyntheticEvent } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useMachine } from '@xstate/react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import contactsMachine from '../machines/ContactsMachine';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    actions: {
      marginTop: theme.spacing(2),
      textAlign: 'right'
    },
    button: {
      marginLeft: '1rem'
    },
    form: {
      display: 'flex',
      flexDirection: 'column'
    },
    status: {
      textAlign: 'center',
      marginTop: theme.spacing(2)
    },
    top: {
      marginTop: theme.spacing(1)
    }
  })
);

function ContactsEdit() {
  const classes = useStyles();
  const { id } = useParams();
  const history = useHistory();
  const [userName, setUserName] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const [validInput, setValidInput] = useState(true);
  const [current, send, service] = useMachine(contactsMachine);
  const { contact } = current.context;

  useEffect(() => {
    setUserName(contact.name);
    setUserEmail(contact.email);
  }, [contact]);

  service.onTransition(state => {
    if (state.matches('idle')) {
      send('SELECT', { contactId: id });
    }
  });

  const changeUserNameHandler = (event: SyntheticEvent) => {
    const inputValue = (event.target as HTMLInputElement).value;
    setUserName(inputValue);
  };

  const changeUserEmailHandler = (event: SyntheticEvent) => {
    const inputValue = (event.target as HTMLInputElement).value;
    setUserEmail(inputValue);
  };

  const editContactHandler = () => {
    if (userName.length === 0 || userEmail.length === 0) {
      setValidInput(false);
      return;
    }

    setValidInput(true);
    send('EDIT', {
      id,
      name: userName,
      email: userEmail
    });
  };

  const cancelContactHandler = () => {
    history.push('/');
  };

  return (
    <Container maxWidth='sm'>
      <Typography variant='h5' gutterBottom>
        Edit user
      </Typography>
      <form className={classes.form}>
        <TextField
          required
          id='name'
          label='Name'
          value={userName}
          onChange={changeUserNameHandler}
        />
        <TextField
          required
          id='email'
          className={classes.top}
          label='Email'
          value={userEmail}
          onChange={changeUserEmailHandler}
        />
        <div className={classes.status}>
          {current.matches('selected.editing') &&
            <CircularProgress size={26} />
          }
          {current.matches('selected.edited') &&
            <Alert severity='success'>The user has been edited successfully.</Alert>
          }
          {!validInput &&
            <Alert severity='error'>Name and email are required fields.</Alert>
          }
          {current.matches('selected.failed') &&
            <Alert severity='error'>There was an error while editing the user.</Alert>
          }
        </div>
        <div className={classes.actions}>
          <Button className={`edit-button ${classes.button}`} onClick={editContactHandler} variant='contained' color='primary'>
            Edit
          </Button>
          <Button className={`cancel-button ${classes.button}`} onClick={cancelContactHandler} variant='contained' color='primary'>
            Cancel
          </Button>
        </div>
      </form>
    </Container>
  );
}

export default ContactsEdit;
