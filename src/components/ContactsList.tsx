import React, { MouseEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { useMachine } from '@xstate/react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import { Contact } from '../models';
import contactsMachine from '../machines/ContactsMachine';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    table: {
      minWidth: 650,
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    title: {
      flex: '1 1 100%',
    },
    loader: {
      display: 'block',
      padding: theme.spacing(2),
      textAlign: 'center'
    },
    link: {
      cursor: 'pointer'
    }
  })
);

function ContactsList() {
  const classes = useStyles();
  const [current, send, service] = useMachine(contactsMachine);
  const { contacts } = current.context;
  const history = useHistory();

  service.onTransition(state => {
    if (state.matches('idle')) {
      send('LOAD');
    }
  });

  const createContactHandler = () => {
    history.push('/contact/create');
  };

  const clickContactHandler = (event: MouseEvent, id: string) => {
    history.push(`/contact/${id}`);
  };

  const editContactHandler = (event: MouseEvent, id: string) => {
    history.push(`/contact/edit/${id}`);
  };

  const deleteContactHandler = (event: MouseEvent, id: string) => {
    send('DELETE', { contactId: id });
    send('LOAD');
  };

  const isLoading = current.matches('loading');
  const failed = current.matches('failure');

  return (
    <Paper>
      <Toolbar className={classes.root}>
        <Typography className={classes.title} variant='h6' id='tableTitle'>
          Contacts
          </Typography>
        <Tooltip title='Create contact'>
          <IconButton onClick={() => createContactHandler()} aria-label='filter list'>
            <AddCircleIcon />
          </IconButton>
        </Tooltip>
      </Toolbar>
      {isLoading || failed ? (
        <>
          {isLoading && (
            <div className={classes.loader}>
              <CircularProgress size={52} />
            </div>
          )}
          {failed && (
            <div className={classes.loader}>
              <Alert severity='error'>There was an error while loading the contacts.</Alert>
            </div>
          )}
        </>
      ) : (
        <TableContainer>
          <Table className={classes.table} aria-label='contacts table'>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align='right'>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {contacts.map((contact: Contact) => (
                <TableRow hover key={contact.id}>
                  <TableCell component='th' scope='row'>
                    <Tooltip title='Contact details'>
                      <Link className={classes.link} onClick={(event: MouseEvent) => clickContactHandler(event, contact.id)}>
                        {contact.name}
                      </Link>
                    </Tooltip>
                  </TableCell>
                  <TableCell align='right'>
                    <Tooltip title='Edit contact'>
                      <IconButton onClick={(event: MouseEvent) => editContactHandler(event, contact.id)}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title='Delete contact'>
                      <IconButton onClick={(event: MouseEvent) => deleteContactHandler(event, contact.id)} aria-label='delete contact'>
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </Paper>
  );
}

export default ContactsList;
