
import axios from 'axios';
import { AnyEventObject } from 'xstate';
import { ContactsContext } from '../models';

export const fetchContacts = async () => {
  const GET_CONTACTS = `
    {
      contacts {
        id
        name
        email
        dateCreated
        dateModified
      }
    }
  `;

  return axios
    .post('/', { query: GET_CONTACTS })
    .then(response => response.data.data);
};

export const fetchContact = (context: ContactsContext, event: AnyEventObject) => {
  const id = event.contactId;
  const GET_CONTACTS = `
    {
      contact (id: "${id}") {
        id
        name
        email
        dateCreated
        dateModified
      }
    }
  `;

  return axios
    .post('/', { query: GET_CONTACTS })
    .then(response => response.data.data);
};

export const createContact = (context: ContactsContext, event: AnyEventObject) => {
  const ADD_CONTACT = `
    mutation ($name: String, $email: String, $dateModified: String, $dateCreated: String) {
      addContact(contact: {name: $name, email: $email, dateModified: $dateModified, dateCreated: $dateCreated}) {
        id
        name
        email
      }
    }
  `;

  const { name, email } = event;

  return axios
    .post('/', {
      query: ADD_CONTACT,
      variables: {
        name,
        email,
        dateModified: new Date(),
        dateCreated: new Date(),
      }
    });
};

export const editContact = (context: ContactsContext, event: AnyEventObject) => {
  const EDIT_CONTACT = `
    mutation ($id: ID, $name: String, $email: String, $dateModified: String) {
      updateContact(contact: {id: $id, name: $name, email: $email, dateModified: $dateModified}) {
        id
        name
        email
      }
    }
  `;

  const { id, name, email } = event;

  return axios
    .post('/', {
      query: EDIT_CONTACT,
      variables: {
        id,
        name,
        email,
        dateModified: new Date(),
      }
    });
};

export const deleteContact = (context: ContactsContext, event: AnyEventObject) => {
  const ADD_CONTACT = `
    mutation ($id: ID) {
      deleteContact(id: $id)
    }
  `;

  return axios
    .post('/', {
      query: ADD_CONTACT,
      variables: {
        id: event.contactId
      }
    });
};