import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

import ContactsList from './components/ContactsList';
import ContactsCreate from './components/ContactsCreate';
import ContactsView from './components/ContactsView';
import ContactsEdit from './components/ContactsEdit';

function App() {
  return (
    <Router>
      <CssBaseline />
      <Switch>
        <Route path='/contact/edit/:id'>
          <ContactsEdit />
        </Route>
        <Route exact path='/contact/create'>
          <ContactsCreate />
        </Route>
        <Route path='/contact/:id'>
          <ContactsView />
        </Route>
        <Route exact path='/'>
          <ContactsList />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
