import { mockedContactsResponse, mockedContactResponse, mockedContactResponseWithError } from './mockedTestData';

export const mockFetchContacts = async () => {
  return mockedContactsResponse;
};

export const mockFetchContact = async (contactId: string) => {
  if (contactId === mockedContactResponse.data.data.contact.id) {
    return mockedContactResponse;
  } else {
    return mockedContactResponseWithError;
  }
};
