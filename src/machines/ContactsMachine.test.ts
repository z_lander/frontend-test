import { interpret } from 'xstate';

import { Contact } from '../models';
import contactsMachine from './ContactsMachine';
import { mockedContactsResponse, mockedContactResponse } from './mockedTestData';
import { mockFetchContacts, mockFetchContact } from './mockedServices';

const testContactsMachine = contactsMachine.withConfig({
  services: {
    fetchContacts: () => mockFetchContacts().then(res => res.data.data),
    fetchContact: (context, event) => mockFetchContact(event.contactId).then(res => res.data.data)
  }
});

describe('contacts machine', () => {
  it('should load contacts on LOAD event', done => {
    const contactsService = interpret(testContactsMachine)
      .onTransition(state => {
        if (state.matches('complete')) {
          expect([mockedContactsResponse.data.data.contacts[0], mockedContactsResponse.data.data.contacts[1]])
            .toEqual([(state.context.contacts as [Contact, Contact])[0], state.context.contacts[1]]);

          done();
        }
      })
      .start();

    contactsService.send('LOAD');
  });

  it('should load contact on SELECT event', done => {
    const contactsService = interpret(testContactsMachine)
      .onTransition(state => {
        if (state.matches({ selected: 'loaded' })) {
          expect(mockedContactResponse.data.data.contact).toEqual(state.context.contact);

          done();
        }
      })
      .start();

    contactsService.send('SELECT', { contactId: 'iE0pnbajZQn2PQqOy1iFn' });
  });
});