
import { Machine, MachineConfig, MachineOptions, assign } from 'xstate';
import { createContact, deleteContact, editContact, fetchContacts, fetchContact } from '../services';
import { ContactsContext, ContactsStateSchema, ContactsEvent, ContactsEventWithData } from '../models';

const ContactsMachineConfig: MachineConfig<
  ContactsContext,
  ContactsStateSchema,
  ContactsEvent
> = {
  id: 'contactsMachine',
  initial: 'idle',
  context: {
    contacts: [],
    contact: {
      id: '',
      email: '',
      name: '',
      dateCreated: '',
      dateModified: ''
    }
  },
  states: {
    idle: {},
    loading: {
      invoke: {
        id: 'fetch-contacts',
        src: 'fetchContacts',
        onDone: {
          target: 'complete',
          actions: 'addLoadedContacts'
        },
        onError: 'failure'
      }
    },
    selected: {
      initial: 'loading',
      states: {
        loading: {
          invoke: {
            id: 'fetch-contact',
            src: 'fetchContact',
            onDone: {
              target: 'loaded',
              actions: 'addLoadedContact'
            },
            onError: 'failed'
          }
        },
        creating: {
          invoke: {
            id: 'create-contact',
            src: 'createContact',
            onDone: {
              target: 'created',
            },
            onError: 'failed'
          },
        },
        editing: {
          invoke: {
            id: 'edit-contact',
            src: 'editContact',
            onDone: {
              target: 'edited',
            },
            onError: 'failed'
          },
        },
        deleting: {
          invoke: {
            id: 'delete-contact',
            src: 'deleteContact',
            onDone: {
              target: 'deleted',
            },
            onError: 'failed'
          },
        },
        loaded: {},
        edited: {},
        created: {},
        deleted: {},
        failed: {}
      }
    },
    complete: {},
    failure: {},
  },
  on: {
    LOAD: {
      target: 'loading'
    },
    SELECT: {
      target: '.selected.loading',
    },
    CREATE: {
      target: '.selected.creating'
    },
    EDIT: {
      target: '.selected.editing'
    },
    DELETE: {
      target: '.selected.deleting'
    }
  }
};

const ContactsMachineOptions: MachineOptions<ContactsContext, ContactsEvent> = {
  actions: {
    addLoadedContacts: assign((ctx: ContactsContext, event: ContactsEvent) => ({
      contacts: (event as ContactsEventWithData).data.contacts,
      contact: ctx.contact
    })),
    addLoadedContact: assign((ctx: ContactsContext, event: ContactsEvent) => ({
      contacts: ctx.contacts,
      contact: (event as ContactsEventWithData).data.contact,
    })),
  },
  guards: {},
  delays: {},
  activities: {},
  services: {
    createContact,
    deleteContact,
    editContact,
    fetchContacts,
    fetchContact
  },
};

export default Machine(ContactsMachineConfig, ContactsMachineOptions);
