export const mockedContactsResponse = {
  data: {
    data: {
      contacts: [
        {
          id: 'iE0pnbajZQn2PQqOy1iFn',
          name: 'Francis Lane',
          email: 'francis@test.com',
          dateCreated: '2020-01-06T09:38:56.805Z',
          dateModified: '2020-01-06T09:38:56.805Z'
        }
        ,
        {
          id: 'VCTOM0RM4g0eREu6fWe-K',
          name: 'Lochlan Mills',
          email: 'mills@gmail.com',
          dateCreated: '2020-01-06T09:38:56.805Z',
          dateModified: '2020-01-06T09:38:56.805Z'
        }
      ]
    }
  }
};

export const mockedContactResponse = {
  data: {
    data: {
      contact: {
        id: 'iE0pnbajZQn2PQqOy1iFn',
        name: 'Francis Lane',
        email: 'francis@test.com',
        dateCreated: '2020-01-06T09:38:56.805Z',
        dateModified: '2020-01-06T09:38:56.805Z'
      }
    }
  }
};

export const mockedContactResponseWithError = {
  data: {
    data: {
      contact: {
        message: 'error'
      }
    }
  }
};
