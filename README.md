# Contact Manager

## Available Scripts

### `npm start`

Runs the app in the development mode on [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run gql`

Runs the graphql server and the playground on [http://localhost:3001](http://localhost:3001).

### `npm test`

Launches the test runner in the interactive watch mode.

## Technologies
The project is build with React `functional components` using `xtate` for app state management. The data is retrieved from the corresponding `graphql` endpoints through the `create-react-app` proxy feature. `Material UI` is used for both components and styles. `Jest` and `Enzyme` for testing. For linting the library of choice is `ESLint` with typescript plug-in.

## Folder Structure
This is partial representation of the folder structure of the project.

```
  +-- src
  |   +-- Index.ts
  |   +-- App.ts
  +-- models
  +-- services
  +-- machines
  |   +-- ContactsMachine.ts
  |   +-- ContactsMachines.test.ts
  +-- components
  |   +-- ContactsCreate.ts
  |   +-- ContactsEdit.ts
  |   +-- ContactsView.ts
  |   +-- ContactsList.ts
  +-- .eslintrc
```

## Nice to Have
* `Typescript` models can be better utilized.
* Caching can be implemented with `xstate` so that the number of `http` requests is reduced.
* Complete test coverage.
* Smarter input field validation.

